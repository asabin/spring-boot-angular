/**
 * Created by Arkady on 04.08.2015.
 */

// Контроллер
// HelloController выполняет функцию контроллера
// функция HelloController получает scope и http, http будет использоваться для ajax запросов
// scope это переменная в которой содержатся все данные, которые есть в нашем документе
// из scope мы можем получить значение которое мы ввели - input поле, оно привязано к полю name,
// соответственно значение которое мы сможем считать из javascript это $scope.name
// соответственно значения которые должны лежать в scope это $scope.person, должно содержать объект который мы отображаем


function HelloController($scope, $http) {
// первый метод который вызывается при запуске этой страицы http get
// http get идет на страницу /hello, передает данные name, это то что отправляется на сервер (/hello?name=Arkady)
    $http.get('hello', {data: {name: name}}).
        success(function (data) {   // После того как с сервера пришел ответ success в переменную data поступают эти данные и в них приходит этот person json объект и мы его помещаем в scope.person и этот scope.person потом будет использоваться для рендеринга нашего шаблона
            $scope.person = data;
        }); // после этого scope.person изменяется, angular отслеживает изменения person и он обновляет данные и мы их видим

    // кнопочка update привязана к методу update
    // он не глобальный, а он тоже привязан к scope (в angular все лежит в scope)
    // какой scope будет использоваться зависит от controller, controller HelloController => scope который в HelloController
    $scope.update = function () {
        // идет на сервер передает в кач-ве параметра имя и обновит данные после того как получит
        $http.get('hello', {params: {name: $scope.name}}).
            success(function (data) {   // при update данные (data) уходят на сервер и возвращаются с сервера в success - функция автоматически вызывается когда данные с сервера придут
                $scope.person = data;
            });
    }
}