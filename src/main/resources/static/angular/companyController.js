/**
 * Created by Arkady on 10.08.2015.
 */

var module = angular.module('myapp', ['ngResource']);

module.factory('User', function ($resource) {   // factory позволяет нам производить объекты определенного типа
    return $resource(':companyName/users', {companyName: '@companyName'});  // метод resource очень удобный метод для работы с rest ресурсами
})  // задаем, что у нас используется companyName, мы его будем подставлять, т.к. может меняться, будем брать его из scope и подставлять сюда,
    // соответственно этот url будет привязан к ресурсу под именем User, таким образом задаем ресурс User
    .controller('CompanyController', function ($scope, $http, User) {
        var url = function () { // нужно дополнительно настроить companyName,  чтобы подставить подходящий companyName
            return {companyName: $scope.companyName || 'company1'};  // либо мы берем его из scope или если он не задан (null или undefined), то по умолчанию
        };

        var update = function () {  // обращается к серверу, отправляет 'companyName/users' запрос
            console.log(url());
            $scope.users = User.query(url());   // Этот User это юзер который определен в factory, и метод query позволяет просто с помощью метода GET пойти по этому адресу (companyName/users) и запросить результат, но нам нужна дополнительная настройка, которую мы делаем с помощью метода url()
        };

        $scope.update = update;

        $scope.add = function () {
            var user = new User();

            user.date = new Date();
            user.name = $scope.name;
            user.password = $scope.password;

            user.$save(url(), function () {    // используем метод save, т.к. user - ресурс, для ркесурса определены стандартные методы, такие как query, save, delete, update etc., метож url() позволяет получить правильный url()
                $scope.name = "";
                $scope.password = "";

                update();
            });
        };

        $scope.delete = function (id) {
            User.delete(angular.extend(url(), {id: id}));
            update();
        };

        $scope.search = function () {
            var company = $scope.companyName || 'company1';
            console.log($scope.userName);
            $http.get(company + '/users/find', {params: {userName: $scope.name}})
                .success(function (res) {
                    console.log(res);
                    $scope.users = res;
                });
        };

        update();
    });