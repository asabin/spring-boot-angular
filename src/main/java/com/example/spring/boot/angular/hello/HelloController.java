package com.example.spring.boot.angular.hello;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Arkady on 04.08.2015.
 */

// Говорит о том, что наш контроллер работает по REST и возвращает данные в формате json,
// для этого использутся автоматическое преобразование данных из java формата в формат json c последующей отправкой на клиента
@RestController
public class HelloController {

    private static final String template = "Hello, %s!";
    private final AtomicInteger counter = new AtomicInteger();

    // Привязываем к этому адресу этот метод
    // Когда переходим по адресу /hello, попадаем в этот метод
    // если name не указано, не передано с клиента, по умолчанию World
    // возвращаем значение типа Person и spring автоматически преобразовывает его в json и возвращает на клиент
    @RequestMapping("/hello")
    public Person hello(@RequestParam(value = "name", defaultValue = "World") String name) {
        return new Person(counter.incrementAndGet(), String.format(template, name));
    }
}