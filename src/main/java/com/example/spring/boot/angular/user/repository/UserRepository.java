package com.example.spring.boot.angular.user.repository;

import com.example.spring.boot.angular.user.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

/**
 * Created by LabeL on 05.08.2015.
 */

public interface UserRepository extends JpaRepository<User, Integer> {
    Collection<User> findByCompanyName(String name);

    Collection<User> findByCompanyNameAndName(String companyName, String userName);
}