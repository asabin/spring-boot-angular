package com.example.spring.boot.angular.user.repository;

import com.example.spring.boot.angular.user.entity.Company;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Created by LabeL on 05.08.2015.
 */

public interface CompanyRepository extends JpaRepository<Company, Integer> {
    Optional<Company> findByName(String name);
}