package com.example.spring.boot.angular;

import com.example.spring.boot.angular.user.entity.Company;
import com.example.spring.boot.angular.user.entity.User;
import com.example.spring.boot.angular.user.repository.CompanyRepository;
import com.example.spring.boot.angular.user.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

import java.util.Date;

// Позволяет стартовать приложение
@SpringBootApplication  // same as @Configuration @EnableAutoConfiguration @ComponentScan
public class Application extends SpringBootServletInitializer {

    @Bean
    CommandLineRunner init(CompanyRepository companyRepository, UserRepository userRepository) {
        return (evt) -> {
            Company company = new Company();
            company.setName("company1");
            companyRepository.save(company);

            User user = new User();

            user.setName("user1");
            user.setPassword("password1");
            user.setDate(new Date());
            user.setCompany(company);

            userRepository.save(user);


            company = new Company();
            company.setName("company2");
            companyRepository.save(company);

            user = new User();

            user.setName("user2");
            user.setPassword("password2");
            user.setDate(new Date());
            user.setCompany(company);

            userRepository.save(user);
        };
    }

    // The first step in producing a deployable war file is to provide a SpringBootServletInitializer subclass and override its configure method.
    // This makes use of Spring Framework’s Servlet 3.0 support and allows you to configure your application when it’s launched by the servlet container.
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(Application.class);
    }

    public static void main(String[] args) {
        // Запускает приложение, поднимает web server (tomcat)
        SpringApplication.run(Application.class, args);
    }
}