package com.example.spring.boot.angular.user.controller;

import com.example.spring.boot.angular.user.entity.Company;
import com.example.spring.boot.angular.user.entity.User;
import com.example.spring.boot.angular.user.repository.CompanyRepository;
import com.example.spring.boot.angular.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

/**
 * Created by LabeL on 05.08.2015.
 */

@RestController
@RequestMapping("{companyName}/users")  // http://localhost:8080/company1/users (эта переменная companyName)
public class CompanyController {

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private UserRepository userRepository;

    @RequestMapping // Не указан http метод, по умолчанию это GET
    private Collection<User> getUsers(@PathVariable String companyName) {   // и эта companyName - одно и то же
        return userRepository.findByCompanyName(companyName);
    }

    @RequestMapping(method = RequestMethod.POST)    // в силу технологии REST у нас адрес один и тот же, а http методы могут быть разные, для метода user.$save по умолчанию вызывается метод POST (это можно переопредилить)
    private void addUser(@PathVariable String companyName, @Valid @RequestBody User user) { // Мы с клиента при отправке, при добавлении User, будем получать json, это json будет отправляться как тело нашего сообщения, и спринг преобразовывает это json в объект User
        Company company = companyRepository.findByName(companyName)
                .orElseThrow(() -> new CompanyNotFoundException(companyName));
//                .orElse(new Company("Arkady"));
        user.setCompany(company);
        userRepository.save(user);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    private void deleteUser(int id) {
        userRepository.delete(id);
    }

    @RequestMapping("find")
    private Collection<User> searchUser(@PathVariable String companyName, String userName) {
        return userRepository.findByCompanyNameAndName(companyName, userName);
    }
}

@ResponseStatus(HttpStatus.NOT_FOUND)
class CompanyNotFoundException extends RuntimeException {
    public CompanyNotFoundException(String companyName) {
        super("Couldn't find company '" + companyName + "'.");
    }
}